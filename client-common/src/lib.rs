use {
    common::Log,
    gilrs::{Axis, Event, EventType, Gilrs},
};

pub struct RCPiClientCommon {
    pub controller: Gilrs,
    pub log: Log,
    pub stick: RCPiClientCommonStick,
}

#[derive(Default)]
pub struct RCPiClientCommonStick {
    pub x: f32,
    pub y: f32,
}

impl RCPiClientCommon {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn trace(&mut self, msg: impl Into<String>) {
        self.log.trace("client", msg);
    }

    pub fn error(&mut self, msg: impl Into<String>) {
        self.log.error("client", msg);
    }

    pub fn check(&mut self) {
        while let Some(Event { event, .. }) = self.controller.next_event() {
            if let EventType::AxisChanged(stick, value, _) = event {
                match stick {
                    Axis::LeftStickX => {
                        self.stick.x = value;
                    }
                    Axis::LeftStickY => {
                        self.stick.y = value;
                    }
                    _ => {}
                }
            }
        }
    }

    pub fn diff(x: f32, y: f32) -> (f32, f32) {
        // #![feature(copysign)]
        // x = y.copysign(x * x);
        // y = y.copysign(y * y);

        if y > 0.0 {
            if x > 0.0 {
                (y - x, y.max(x))
            } else {
                (y.max(-x), y + x)
            }
        } else if x > 0.0 {
            (-((-y).max(x)), y + x)
        } else {
            (y - x, -((-y).max(-x)))
        }
    }
}

impl Default for RCPiClientCommon {
    fn default() -> Self {
        let mut log = Log::new("10.0.0.25:8082");

        log.info("client", "Starting RCPi client");

        Self {
            controller: Gilrs::new().unwrap(),
            log,
            stick: RCPiClientCommonStick::default(),
        }
    }
}
