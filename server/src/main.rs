#[cfg(target_os = "linux")]
use {
    common::{ClientMessage, Log, ServerMessage, BUFFER_SIZE},
    rscam::{Camera, Config},
    std::{io::Write, net::UdpSocket, thread, time::Duration},
};

#[cfg(not(target_os = "linux"))]
fn main() {}

#[cfg(target_os = "linux")]
fn main() {
    RCPiServer::new().run();
}

#[cfg(target_os = "linux")]
struct RCPiServer {
    log: Log,
    socket: UdpSocket,
}

#[cfg(target_os = "linux")]
impl RCPiServer {
    fn new() -> Self {
        let socket = UdpSocket::bind("0.0.0.0:8080").expect("Unable to bind to socket");

        let mut log = Log::new("10.0.0.25:8082");

        log.info("server", "Starting RCPi server");

        Self { log, socket }
    }

    fn trace(&mut self, msg: impl Into<String>) {
        self.log.trace("server", msg);
    }

    fn error(&mut self, msg: impl Into<String>) {
        self.log.error("server", msg);
    }

    fn run(&mut self) {
        let mut state_send_buffer: Vec<u8> = Vec::new();
        let mut state_recv_buffer = [0u8; BUFFER_SIZE as usize];

        let mut camera = Camera::new("/dev/video0").expect("No camera mounted");

        println!("Starting camera");
        camera
            .start(&Config {
                interval: (1, 30),
                resolution: (320, 240),
                format: b"MJPG",
                ..Default::default()
            })
            .expect("Unable to start camera");

        loop {
            match self.socket.recv_from(&mut state_recv_buffer) {
                Ok((size, addr)) => {
                    match serde_json::from_slice::<ClientMessage>(&state_recv_buffer[..size]) {
                        Ok(state) => {
                            self.trace(format!("{:?}", state));

                            match state {
                                ClientMessage::State { send, .. } => {
                                    if send {
                                        let mut image: Vec<u8> = Vec::with_capacity(76800);

                                        let frame =
                                            camera.capture().expect("Error captures camera frame");
                                        image
                                            .write_all(&frame[..])
                                            .expect("Image buffer couldn't hold captured frame");

                                        if let Err(err) = serde_json::to_writer(
                                            &mut state_send_buffer,
                                            &ServerMessage::StateImage {
                                                cpu: 1.0,
                                                ram: 1.0,
                                                left: 1.0,
                                                right: 1.0,
                                                image,
                                            },
                                        ) {
                                            self.error(format!(
                                                "Unable to serialize state: {}",
                                                err
                                            ));

                                            break;
                                        }

                                        if let Err(err) =
                                            self.socket.send_to(&state_send_buffer[..], addr)
                                        {
                                            self.error(format!(
                                                "Unable to send state data: {}",
                                                err
                                            ));

                                            break;
                                        }
                                    } else {
                                        if let Err(err) = serde_json::to_writer(
                                            &mut state_send_buffer,
                                            &ServerMessage::State {
                                                cpu: 1.0,
                                                ram: 1.0,
                                                left: 1.0,
                                                right: 1.0,
                                            },
                                        ) {
                                            self.error(format!(
                                                "Unable to serialize state: {}",
                                                err
                                            ));

                                            break;
                                        }

                                        if let Err(err) =
                                            self.socket.send_to(&state_send_buffer[..], addr)
                                        {
                                            self.error(format!(
                                                "Unable to send state data: {}",
                                                err
                                            ));

                                            break;
                                        }
                                    }
                                }
                                _ => break,
                            }

                            state_send_buffer.clear();
                        }
                        Err(err) => {
                            self.error(format!("Unable to parse state data: {}", err));
                        }
                    }
                }
                Err(err) => {
                    self.error(format!("Could not receive data: {}", err));
                }
            }

            thread::sleep(Duration::from_millis(10));
        }
    }
}
