use {
    client_common::RCPiClientCommon,
    common::{ClientMessage, ServerMessage, BUFFER_SIZE, IMAGE_SIZE},
    glium::{
        backend::Facade,
        glutin::{dpi::LogicalSize, ContextBuilder, EventsLoop, WindowBuilder},
        texture::{ClientFormat, RawImage2d},
        Display, Surface, Texture2d,
    },
    image::{jpeg::JPEGDecoder, ImageDecoder},
    imgui::{ImFontConfig, ImGui, ImGuiCol, ImGuiCond, ImTexture, StyleVar},
    imgui_glium_renderer::Renderer,
    std::{
        borrow::Cow,
        io::Cursor,
        net::UdpSocket,
        sync::{
            atomic::{AtomicBool, Ordering},
            Arc,
        },
        thread,
        time::{Duration, Instant},
    },
};

const SIZE: (f64, f64) = (1280.0, 720.0);

fn main() {
    RCPiClient::new().run();
}

struct RCPiClient {
    client_send_buffer: Vec<u8>,
    client_common: RCPiClientCommon,
    gui: RCPiClientGui,
    quit: Arc<AtomicBool>,
    socket: UdpSocket,
    server: RCPiClientServer,
    server_recv_buffer: Vec<u8>,
    texture_id: Option<ImTexture>,
}

struct RCPiClientGui {
    colors: Vec<(ImGuiCol, (f32, f32, f32, f32))>,
    display: Display,
    events_loop: EventsLoop,
    imgui: ImGui,
    instant: Instant,
    renderer: Renderer,
    size: LogicalSize,
    styles: Vec<StyleVar>,
}

#[derive(Default)]
struct RCPiClientServer {
    cpu: f32,
    ram: f32,
    left: f32,
    right: f32,
    image: Option<Vec<u8>>,
}

impl RCPiClient {
    fn new() -> Self {
        let socket = UdpSocket::bind("0.0.0.0:8081").expect("Unable to bind to socket");
        socket
            .connect("10.0.0.21:8080")
            .expect("Unable to connect to remote server");
        socket
            .set_nonblocking(true)
            .expect("Unable to make the UDP socket nonblocking");

        let client_common = RCPiClientCommon::new();

        let events_loop = EventsLoop::new();
        let context = ContextBuilder::new().with_vsync(true);
        let builder = WindowBuilder::new()
            .with_title("RCPi")
            .with_dimensions(LogicalSize::new(SIZE.0, SIZE.1));
        let display = Display::new(builder, context, &events_loop).unwrap();

        let mut imgui = ImGui::init();
        imgui.set_ini_filename(None);

        imgui.fonts().add_default_font_with_config(
            ImFontConfig::new()
                .oversample_h(1)
                .pixel_snap_h(true)
                .size_pixels((13.0 * display.gl_window().get_hidpi_factor().round()) as f32),
        );

        imgui.set_font_global_scale((1.0 / display.gl_window().get_hidpi_factor().round()) as f32);

        let renderer = Renderer::init(&mut imgui, &display).expect("Failed to initialize renderer");

        imgui_winit_support::configure_keys(&mut imgui);

        Self {
            client_send_buffer: Vec::new(),
            client_common,
            gui: RCPiClientGui {
                colors: vec![(ImGuiCol::FrameBg, (0.0, 0.0, 0.0, 0.0))],
                display,
                events_loop,
                imgui,
                instant: Instant::now(),
                renderer,
                size: LogicalSize::new(SIZE.0, SIZE.1),
                styles: vec![StyleVar::WindowRounding(0.0)],
            },
            quit: Arc::new(AtomicBool::new(false)),
            socket,
            server: RCPiClientServer::default(),
            server_recv_buffer: Vec::with_capacity(BUFFER_SIZE as usize),
            texture_id: None,
        }
    }

    #[allow(dead_code)]
    fn trace(&mut self, msg: impl Into<String>) {
        self.client_common.trace(msg);
    }

    #[allow(dead_code)]
    fn error(&mut self, msg: impl Into<String>) {
        self.client_common.error(msg);
    }

    fn register_texture(&mut self) {
        if let Some(image) = &self.server.image {
            let byte_stream = Cursor::new(image);
            let decoder = JPEGDecoder::new(byte_stream).expect("Image data not a valid JPEG");
            let image = decoder
                .read_image()
                .expect("Unable to read image from buffer");

            let raw = RawImage2d {
                data: Cow::Owned(image),
                width: u32::from(IMAGE_SIZE.0),
                height: u32::from(IMAGE_SIZE.1),
                format: ClientFormat::U8U8U8,
            };

            let gl_texture = Texture2d::new(self.gui.display.get_context(), raw)
                .expect("Unable to create textures");
            let texture_id = self.gui.renderer.textures().insert(gl_texture);

            self.texture_id = Some(texture_id);
        }
    }

    fn check(&mut self) {
        let imgui = &mut self.gui.imgui;
        let gui_size = &mut self.gui.size;
        let hidpi_factor = self.gui.display.gl_window().get_hidpi_factor();
        let quit = self.quit.clone();

        self.client_common.check();

        self.gui.events_loop.poll_events(|event| {
            use glium::glutin::{Event, WindowEvent};

            imgui_winit_support::handle_event(
                &mut (*imgui),
                &event,
                hidpi_factor,
                hidpi_factor.round(),
            );

            if let Event::WindowEvent { event, .. } = event {
                match event {
                    WindowEvent::Resized(size) => {
                        *gui_size = size;
                    }
                    WindowEvent::CloseRequested => {
                        quit.store(true, Ordering::SeqCst);
                    }
                    _ => {}
                }
            }
        });

        match self.socket.recv_from(&mut self.server_recv_buffer) {
            Ok((size, _addr)) => {
                match serde_json::from_slice::<ServerMessage>(&self.server_recv_buffer[..size]) {
                    Ok(msg) => match msg {
                        ServerMessage::StateImage {
                            cpu,
                            ram,
                            left,
                            right,
                            image,
                        } => {
                            self.server.cpu = cpu;
                            self.server.ram = ram;
                            self.server.left = left;
                            self.server.right = right;
                            self.server.image = Some(image);
                        }
                        ServerMessage::State {
                            cpu,
                            ram,
                            left,
                            right,
                        } => {
                            self.server.cpu = cpu;
                            self.server.ram = ram;
                            self.server.left = left;
                            self.server.right = right;
                        }
                    },
                    Err(err) => {
                        self.error(format!("Unable to parse state data: {}", err));
                    }
                }
            }
            Err(err) => match err.kind() {
                std::io::ErrorKind::WouldBlock => {}
                _ => self.error(format!("Could not receive data: {}", err)),
            },
        }
    }

    fn handle(&mut self, state: &mut ClientMessage) {
        let (l, r) = RCPiClientCommon::diff(self.client_common.stick.x, self.client_common.stick.y);

        if let ClientMessage::State { left, right, .. } = state {
            *left = l;
            *right = r;
        }
    }

    fn ui(&mut self) {
        let now = Instant::now();
        let delta = now - self.gui.instant;
        let delta_s = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1_000_000_000.0;
        self.gui.instant = now;

        imgui_winit_support::update_mouse_cursor(&self.gui.imgui, &self.gui.display.gl_window());

        let frame_size = imgui_winit_support::get_frame_size(
            &self.gui.display.gl_window(),
            self.gui.display.gl_window().get_hidpi_factor().round(),
        )
        .expect("Unable to get frame size");

        let ui = self.gui.imgui.frame(frame_size, delta_s);
        let texture_id = &mut self.texture_id;
        let _quit = self.quit.clone();
        let size = &mut self.gui.size;

        // ui.with_style_and_color_vars(&self.gui.styles[..], &self.gui.colors[..], || {
        //     ui.main_menu_bar(|| {
        //         ui.menu_bar(|| {
        //             ui.menu(imgui::im_str!("File")).build(|| {
        //                 quit.store(
        //                     ui.menu_item(imgui::im_str!("Quit")).build(),
        //                     Ordering::SeqCst,
        //                 );
        //             })
        //         });
        //     });

        ui.with_style_and_color_vars(&self.gui.styles[..], &self.gui.colors[..], || {
            ui.window(imgui::im_str!("Controls"))
                .size(
                    (
                        Self::percent(20.0, size.width) as f32,
                        Self::percent(60.0, size.height) as f32,
                    ),
                    ImGuiCond::Always,
                )
                .position((0.0, 0.0), ImGuiCond::Always)
                .resizable(false)
                .collapsible(false)
                .build(|| {
                    ui.text(imgui::im_str!("FPS: {}", ui.framerate()));
                });

            ui.window(imgui::im_str!("Camera"))
                .size(
                    (
                        Self::percent(80.0, size.width) as f32,
                        Self::percent(60.0, size.height) as f32,
                    ),
                    ImGuiCond::Always,
                )
                .position(
                    (Self::percent(20.0, size.width) as f32, 0.0),
                    ImGuiCond::Always,
                )
                .resizable(false)
                .collapsible(false)
                .build(|| {
                    if let Some(id) = texture_id {
                        ui.image(*id, (f32::from(IMAGE_SIZE.0), f32::from(IMAGE_SIZE.1)))
                            .build();
                    }
                });

            ui.window(imgui::im_str!("Status"))
                .size(
                    (
                        Self::percent(40.0, size.width) as f32,
                        Self::percent(60.0, size.height) as f32,
                    ),
                    ImGuiCond::Always,
                )
                .position(
                    (0.0, Self::percent(60.0, size.height) as f32),
                    ImGuiCond::Always,
                )
                .resizable(false)
                .collapsible(false)
                .build(|| {
                    ui.text(imgui::im_str!("FPS: {}", ui.framerate()));
                });

            ui.window(imgui::im_str!("Log"))
                .size(
                    (
                        Self::percent(60.0, size.width) as f32,
                        Self::percent(40.0, size.height) as f32,
                    ),
                    ImGuiCond::Always,
                )
                .position(
                    (
                        Self::percent(40.0, size.width) as f32,
                        Self::percent(60.0, size.height) as f32,
                    ),
                    ImGuiCond::Always,
                )
                .resizable(false)
                .collapsible(false)
                .build(|| {
                    ui.text(imgui::im_str!("FPS: {}", ui.framerate()));
                });
        });

        let mut target = self.gui.display.draw();
        target.clear_color(0.42, 0.42, 0.42, 1.0);

        if let Err(err) = self.gui.renderer.render(&mut target, ui) {
            self.error(format!("Unable to finish render: {}", err));
        }

        if let Err(err) = target.finish() {
            self.error(format!("Unable to finish target: {}", err));
        }
    }

    fn run(&mut self) {
        let mut state = ClientMessage::State {
            left: 0.0,
            right: 0.0,
            send: true,
        };

        loop {
            self.check();
            self.handle(&mut state);
            self.register_texture();
            self.ui();

            if let Err(err) = serde_json::to_writer(&mut self.client_send_buffer, &state) {
                self.error(format!("Unable to serialize state: {}", err));

                break;
            }

            if let Err(err) = self.socket.send(&self.client_send_buffer[..]) {
                self.error(format!("Unable to send state data: {}", err));

                break;
            }

            self.client_send_buffer.clear();

            if self.quit.load(Ordering::SeqCst) {
                break;
            }

            thread::sleep(Duration::from_millis(10));
        }
    }

    fn percent(per: f64, size: f64) -> f64 {
        (per / 100.0) * size
    }
}
