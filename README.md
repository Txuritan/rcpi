# RCPi (Remote Controlled Pi)

This is the source code to my robot that started two years after my time as team leader for a FRC team.

After I left I knew I had to make my own robot and the official FRC parts are too expensive, so I went with the Raspberry Pi 3B+.

RCPi consists of three parts, the common library, the client, and the server.
THe client does all the heavy lifting as to not stress the Pi (and so I don't have to recompile the server every time I want to change something).

The client connects to the server's UDP socket, looks for a controller (XBox only as of now), creates a default (zeroed) pin out state, starts a loop that check for a controller update, does what ever logic is needed, takes the pin out state and serializes it into json, this json is then sent to the server over the UDP socket.

## Robot Parts

- [2 - Cytron Motor Controller MD30C](https://www.cytron.io/p-30amp-5v-30v-dc-motor-driver)
- [4 - GP Battery 12V/1.2Ah SLA-12-12](https://www.cytron.io/c-lead-acid-rechargeable-battery-and-charger/p-12v-1.2ah-lead-acid)
- [2 - Electric Scooter Motor With Gearbox](https://www.cytron.io/p-24v-395rpm-112kgfcm-electric-scooter-motor-with-gear)