use {
    chrono::prelude::*,
    serde::{Deserialize, Serialize},
    std::net::{ToSocketAddrs, UdpSocket},
};

pub const BUFFER_SIZE: u32 = 1024;
pub const IMAGE_SIZE: (u16, u16) = (320, 240);

#[derive(serde::Deserialize, serde::Serialize)]
pub struct Msg {
    pub side: String,
    pub level: String,
    pub time: DateTime<Utc>,
    pub message: String,
}

pub struct Log {
    socket: UdpSocket,
}

impl Log {
    pub fn new(uri: impl ToSocketAddrs) -> Self {
        let socket = UdpSocket::bind("0.0.0.0:8083")
            .expect("Unable to create UDP socket, check the running port");

        socket.connect(uri).expect("Unable to connect to socket");

        Self { socket }
    }

    pub fn log(&mut self, msg: Msg) -> std::io::Result<()> {
        let bytes = serde_json::to_vec(&msg)?;

        self.socket.send(&bytes[..])?;

        Ok(())
    }

    pub fn error(&mut self, side: impl Into<String>, msg: impl Into<String>) {
        self.log(Msg {
            side: side.into(),
            level: "error".into(),
            time: Utc::now(),
            message: msg.into(),
        })
        .expect("unable to send redis error log");
    }

    pub fn info(&mut self, side: impl Into<String>, msg: impl Into<String>) {
        self.log(Msg {
            side: side.into(),
            level: "info".into(),
            time: Utc::now(),
            message: msg.into(),
        })
        .expect("unable to send redis info log");
    }

    pub fn trace(&mut self, side: impl Into<String>, msg: impl Into<String>) {
        self.log(Msg {
            side: side.into(),
            level: "trace".into(),
            time: Utc::now(),
            message: msg.into(),
        })
        .expect("unable to send redis trace log");
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ServerMessage {
    State {
        cpu: f32,
        ram: f32,
        left: f32,
        right: f32,
    },
    StateImage {
        cpu: f32,
        ram: f32,
        left: f32,
        right: f32,
        image: Vec<u8>,
    },
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ClientMessage {
    State { left: f32, right: f32, send: bool },
    Stop,
}
