use {
    arrayvec::ArrayVec,
    common::Msg,
    mio::{net::UdpSocket, Events, Poll, PollOpt, Ready, Token},
    rusqlite::{types::ToSql, Connection},
    std::{
        sync::mpsc::{channel, TryRecvError},
        thread,
        time::{Duration, Instant},
    },
};

const SOCKET: Token = Token(0);

fn main() -> Result<(), Box<std::error::Error>> {
    simple_logger::init_with_level(log::Level::Info)?;

    let mut conn = Connection::open("./log.db")?;

    conn.execute_batch("CREATE TABLE IF NOT EXISTS log (side TEXT NOT NULL, level TEXT NOT NULL, time TEXT NOT NULL, message TEXT NOT NULL);")?;

    let (tx, rx) = channel::<Msg>();

    thread::spawn(move || {
        let mut back = ArrayVec::<[Msg; 64]>::new();

        let mut now = Instant::now();

        loop {
            if back.is_full() || Instant::now().duration_since(now).as_secs() >= 5 {
                let trans = conn
                    .transaction()
                    .expect("Unable to create SQLite transaction");

                {
                    let mut stmt = trans
                        .prepare_cached(
                            "INSERT INTO log(side, level, time, message) VALUES (?1, ?2, ?2, ?4);",
                        )
                        .expect("Unable to prepare SQLite statement");

                    for msg in back.iter() {
                        stmt.execute(&[
                            &msg.side as &ToSql,
                            &msg.level as &ToSql,
                            &msg.time as &ToSql,
                            &msg.message as &ToSql,
                        ])
                        .expect("Unable to execute SQLite statement");
                    }
                }

                trans.commit().expect("Unable to commit SQLite transaction");

                back.clear();
            }

            match rx.try_recv() {
                Ok(recv) => {
                    if back.is_full() {
                        let trans = conn
                            .transaction()
                            .expect("Unable to create SQLite transaction");

                        {
                            let mut stmt = trans
                                .prepare_cached(
                                    "INSERT INTO log(side, level, time, message) VALUES (?1, ?2, ?2, ?4);",
                                )
                                .expect("Unable to prepare SQLite statement");

                            for msg in back.iter() {
                                stmt.execute(&[
                                    &msg.side as &ToSql,
                                    &msg.level as &ToSql,
                                    &msg.time as &ToSql,
                                    &msg.message as &ToSql,
                                ])
                                .expect("Unable to execute SQLite statement");
                            }
                        }

                        trans.commit().expect("Unable to commit SQLite transaction");

                        back.clear();
                    } else {
                        back.push(recv);
                        now = Instant::now();
                    }
                }
                Err(err) => {
                    if let TryRecvError::Disconnected = err {
                        log::error!("Unable to recv message, sender disconnected: {}", err)
                    }
                }
            }

            thread::sleep(Duration::from_millis(5));
        }
    });

    let mut buff = [0; 128];

    let poll = Poll::new()?;
    let mut events = Events::with_capacity(500);
    let socket = UdpSocket::bind(&"0.0.0.0:8082".parse()?)?;
    poll.register(&socket, SOCKET, Ready::readable(), PollOpt::edge())?;

    loop {
        poll.poll(&mut events, Some(Duration::from_millis(100)))?;

        for event in events.iter() {
            match event.token() {
                SOCKET => {
                    let size = socket.recv(&mut buff)?;
                    let msg = serde_json::from_slice(&buff[0..size])?;
                    tx.send(msg)?;
                }
                _ => unreachable!(),
            }
        }
    }
}
